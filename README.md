# EverlastingFC
[Visit Site](https://everlastingfc.now.sh/)

## Intro

This is a side project utilizing the [Gridsome](https://gridsome.org/) framework. The goal of this project was to create a fun way for me to learn and use Gridsome, GraphQL, an external API, and Vue basics.

Final Fantasy XIV is a video game that I play when I get the chance. Once I found out that there was an API created for the game, I figured that I could turn this into a fun project! As I mentioned earlier, I learn the best when I have fun projects, so here it is.

This is a simple site that features Everlasting, the Free Company that I'm a part of within the game. It also features individual player profiles for each of the members in the Free Company. In case you're unfamiliar, a Free Company is a group of players that belong to and can socialize or team up with.

## Goals

- Continue to familiarize with Gridsome and GraphQL
- Work with an external API
- Work with cloudinary remote images
- Create a Free Company site to help attract more members
- Create dynamic character pages and a dynamic homepage via the XIVAPI API.

## Tech Used

- Vue / Javascript
- GraphQL
- HTML
- CSS
- Figma for mockups

## Services Used

- [Cloudinary](https://cloudinary.com/) -- Image Hosting
- [GitLab](https://gitlab.com/) -- Git Repo
- [Zeit.co](https://zeit.co/) -- Static File Hosting and deployment
- [XIVAPI](https://xivapi.com/) -- Final Fantasy XIV Data API
- [Zapier](https://zapier.com/home) -- Automation tool to rebuild site daily
