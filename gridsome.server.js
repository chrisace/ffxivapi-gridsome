const fetch = require("node-fetch");

module.exports = function (api) {
  api.loadSource(async actions => {
    const membersCollection = actions.addCollection('Members')
    const fcCollection = actions.addCollection('Fc')
    const charCollection = actions.addCollection('Char')
    const mountCollection = actions.addCollection('Mounts')
    const minionCollection = actions.addCollection('Minions')
    await ff14(membersCollection, fcCollection, mountCollection, minionCollection, charCollection)
  })
}

//ff14 data
async function ff14(collection, collection2, collection3, collection4, collection5) {

  const urls = [
    'https://xivapi.com/freecompany/9237305048202071824?data=FCM',
    'https://xivapi.com/search?indexes=Mount&columns=ID,Icon,IconSmall,Name&filters=Order%3E=0&limit=1000',
    'https://xivapi.com/search?indexes=Companion&columns=ID,Icon,IconSmall,Name&filters=Order%3E=0&limit=1000',
    'https://xivapi.com/search?indexes=Companion&columns=ID,Icon,IconSmall,Name&filters=Order%3E=0&limit=999&page=2'
  ]

  const promises = urls.map(url => fetch(url, { mode: 'cors' }).then(r => r.json()))
  const [fc, mounts, minions, minions2] = await Promise.all(promises)
  const memberIds = [];

  // Add Members from FC List
  for (const member of fc.FreeCompanyMembers) {
    collection.addNode({
      member: member
    })
    memberIds.push(member.ID)
  }
  console.log('Finished creating member list')

  // Add FC info
  collection2.addNode({
    fcInfo: fc.FreeCompany,
  })
  console.log('Finished Creating FC Info Data')

  // Add Mounts Data
  collection3.addNode({
    mounts: mounts.Results,
  })
  console.log('Finished Creating Mount Data')

  // Add Minions Data
  const minionData = [...minions.Results, ...minions2.Results ]
  collection4.addNode({
    minions: minionData,
  })
  console.log('Finished Creating Minion Data')


  // Group member ids by 12 to prevent promise.all from reaching the API limit per second
  // i'm sure thjis section could be done better, but i cant think of a better way how atm. This works though
  const groupedMemberIds = []
  let limit = 12
  let tempArr = []
  let newArr = []

  for (const [i, id] of memberIds.entries()) {
    if (i % limit === 0) {
      tempArr.length > 0 ? newArr.push(tempArr) : ''
      tempArr = []
      tempArr.push(id)
    } else {
      tempArr.push(id)
    }
  }
  if (tempArr.length > 0) {
    newArr.push(tempArr)
  }
  groupedMemberIds.push(...newArr)

  // Loop through each group of 12
  for (const [index,group] of groupedMemberIds.entries()) {

    console.log('working on group ' + (index+1))
    const promises = group.map(id => fetch('https://xivapi.com/character/' + id + '?data=CJ,MIMO&extended=1', { mode: 'cors' }).then(r => r.json()))
    const charData = await Promise.all(promises)
    for (const char of charData) {
      collection5.addNode({
        minions: char.Minions,
        mounts: char.Mounts,
        character: char.Character,
        id: char.Character.ID,
      })
    }

  }


}
