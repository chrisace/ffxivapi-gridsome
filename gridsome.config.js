// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'Everlasting FC',
  siteDescription: 'An awesome Final Fantasy XIV Free Company located in Sargatanas.',
  templates: {
    // ff14Member: '/char/:id',
    Char: [
      {
        path: '/char/:id',
        component: './src/templates/Char.vue'
      },
    ],
  },
  plugins: [
    {
      use: '@gridsome/plugin-google-analytics',
      options: {
        id: 'UA-160250344-1'
      }
    }
  ],
  chainWebpack: config => {
    config.resolve.alias.set('@images', '~/images')
    // config.resolve.alias.set('@images', '@/assets/images')
  },
  
}
