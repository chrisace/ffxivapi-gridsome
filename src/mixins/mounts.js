export const mounts = {
  props: {
    mainTitle: {
      type: String,
      default: 'Mounts'
    },
    mounts: {
      type: Array,
      required: true
    },
    big: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    baseData() {
      return this.$static.mnts.edges[0].node.mounts
    },
  },
}