export const helpers = {
  methods: {
    underscore(val) {
      return val
        .split(" ")
        .join("_")
        .toLowerCase();
    }
  }
};

export const jobIds = {
  data() {
    return {
      jobIds: {
        tank: {
          name: "Tank",
          ids: {
            tank: {
              name: "Tank",
              values: [1, 3, 32, 37]
            }
          }
        },
        dps: {
          name: "DPS",
          ids: {
            physical_ranged: {
              name: "Physical Ranged",
              values: [5, 31, 38]
            },
            melee: {
              name: "Melee",
              values: [2, 4, 29, 34]
            },
            magical_ranged: {
              name: "Magical Ranged",
              values: [7, 26, 35, 36]
            }
          }
        },
        healer: {
          name: "Healer",
          ids: {
            healer: {
              name: "Healer",
              values: [6, 26, 33]
            }
          }
        },
        craft: {
          name: "Craft",
          ids: {
            diciples_of_the_hand: {
              name: "Diciples of the Hand",
              values: [8, 9, 10, 11, 12, 13, 14, 15]
            },
            diciples_of_the_land: {
              name: "Diciples of the Land",
              values: [16, 17, 18]
            }
          }
        }
      }
    };
  }
};

export const statCats = {
  data() {
    return {
      statCategories: [
        {
          ids: [1, 2, 3, 4, 5],
          image: "attr.png",
          name: "Attributes"
        },
        {
          ids: [27, 44, 22],
          image: "offensive.png",
          name: "Offensive Properties"
        },
        {
          ids: [21, 24],
          image: "defensive.png",
          name: "Defensive Properties"
        },
        {
          ids: [20, 45],
          image: "physical.png",
          name: "Physical Properties"
        },
        {
          ids: [33, 34, 46],
          image: "mental.png",
          name: "Mental Properties"
        },
        {
          ids: [19, 6],
          image: "attr.png",
          name: "Role"
        },
        {
          ids: [7, 8],
          image: "attr.png",
          name: "Health & Magic"
        }
      ]
    };
  }
};
