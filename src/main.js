// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
require("typeface-rubik");
require("typeface-roboto");

import "~/assets/css/style.css";
import NProgress from "nprogress";
import "nprogress/nprogress.css";

import DefaultLayout from "~/layouts/Default.vue";

export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component("Layout", DefaultLayout);

  // head.link.push({
  //   rel: 'canonical',
  //   href: `https://everlastingfc.now.sh/${this.$route.path}`
  // })

  router.beforeEach((to, from, next) => {
    if (!to.hash && typeof document !== "undefined") {
      NProgress.start();
    }
    next();
  });

  router.afterEach((to, from) => {
    if (!to.hash && typeof document !== "undefined") {
      NProgress.done();
    }
  });
}
